﻿using MediatR;
using Milestone.Models;

namespace Milestone.Commands
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
    }
}
