﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Milestone.Commands;
using Milestone.Models;
using Milestone.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Milestone.Controllers
{
    [Route("api/")]
    [ApiController]
    public class GroceryController : ControllerBase
    {

        /*
        IGroceries _groceriesservices;

        public GroceryController(IGroceries gservices)
        {
            _groceriesservices = gservices;
        }
        */

        IMediator mediator;

        public GroceryController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("get-all-menubar")]
        public async Task<IEnumerable<MenuBar>> GetMenu()
        {
            var temp = await mediator.Send(new GetMenuBarQuery());
            return temp;

        }

        [HttpGet]
        [Route("get-product-details")]
        public async Task<IEnumerable<Product>> GetProduct()
        {
            var temp = await mediator.Send(new GetProductQuery());
            return temp;

        }

        [HttpGet]
        [Route("get-product-by-id/{id}")]
        public async Task<Product> GetProductById(int id)
        {
            var temp = await mediator.Send(new GetProductByIdQuery() { Id = id });
            return temp;

        }

        [HttpGet]
        [Route("get-order-by-id/{id}")]
        public async Task<ProductOrder> GetOrderById(int id)
        {
            var temp = await mediator.Send(new GetOrderInfoQuery() { OrderId = id });
            return temp;

        }

        [HttpPost]
        [Route("add-product")]
        public async Task<IEnumerable<Product>> AddProduct([FromBody] Product newproduct)
        {
            var temp = await mediator.Send(new AddProductCommand() { Product = newproduct });
            return temp;

        }


        [HttpPost]
        [Route("place-order")]
        public async Task<ProductOrder> Placeorder(int userId, int productId)
        {
            return await mediator.Send(new PlaceOrderCommand() { UserId = userId, ProductId = productId });
        }

    }
}