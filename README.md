# Groceries Management System

This project is developed using .Net 3.1 and follows CQRS Architecture pattern,
it uses MS SQL Database with 5 Tables,CRUD operations are implemented 
* Menu Bar
* Application User
* Product
* Product Order
* Categories 

![Image](Thejaswin_Milestone_9-Sep-3pm/API.JPG)


## API Reference

#### Get all items in Menubar Table

```http
  GET /api/get-all-menubar
```

| Parameter | Response |
| :-------- | :------- |
| None | `List of items in Menubar` |

#### Get all items in Product Table

```http
  GET /api/get-product-details
```

| Parameter | Response |
| :-------- | :------- |
| None | `List of items in Product` |

#### Get specific product by id in Product Table

```http
  GET /api/get-product-by-id/{id}
```

| Parameter | Response |
| :-------- | :------- |
| `id`| `Returns the product with specified id`|


#### Get specific order by id in Produc Order Table

```http
  GET /api/get-order-by-id/{id}
```

| Parameter | Response |
| :-------- | :------- |
| `id`| `Returns the order with specified id`|

#### Post new Product in Product Table

```http
  POST /api/add-product
```

| Parameter | Response |
| :-------- | :------- |
| `Product Object`| `List of Product`|

#### Place new Order
```http
  POST /api/place-order
```

| Parameter | Response |
| :-------- | :------- |
| `UserId,ProductId`| `List of ProductOrder`|


