﻿using Milestone.BusinessLayer.Services;
using Milestone.Models;
using System.Collections.Generic;
using System.Linq;


namespace Milestone.BusinessLayer.Interface
{
    public class GroceriesServices : IGroceries
    {

        private GroceryDbContext _dbContext;

        public GroceriesServices(GroceryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<MenuBar> GetAllCategories()
        {
            return _dbContext.MenuBar.ToList();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _dbContext.Product.ToList();

        }

        public ProductOrder GetOrderInfo(int id)
        {
            return _dbContext.ProductOrder.SingleOrDefault(x => x.OrderId == id);
        }

        public Product GetProductById(int id)
        {
            return _dbContext.Product.SingleOrDefault(x => x.ProductId == id);

        }

        public IEnumerable<Product> AddProduct(Product newProduct)
        {
            _dbContext.Product.Add(newProduct);
            _dbContext.SaveChanges();
            return _dbContext.Product.ToList();
        }

        public ProductOrder PlaceOrder(int userId, int productId)
        {
            var order = new ProductOrder()
            {
                OrderId = _dbContext.ProductOrder.Max(x => x.OrderId) + 1,
                UserId = userId,
                ProductId = productId,
                User = _dbContext.ApplicationUser.SingleOrDefault(x => x.UserId == userId),
                Product = _dbContext.Product.SingleOrDefault(x => x.ProductId == productId)
            };

            _dbContext.ProductOrder.Add(order);
            _dbContext.SaveChanges();
            return order;
        }
    }
}

