﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Models;
using Milestone.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        IGroceries _data;

        public GetProductByIdHandler(IGroceries data)
        {
            _data = data;
        }
        public async Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetProductById(request.Id));
        }
    }
}
