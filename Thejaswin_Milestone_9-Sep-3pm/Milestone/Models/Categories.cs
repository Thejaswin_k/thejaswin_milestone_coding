﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Milestone.Models
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        [Required]
        public int CatId { get; set; }
        [Required]
        public string CatName { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
