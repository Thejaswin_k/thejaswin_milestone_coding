﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Models;
using Milestone.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class GetProductHandler : IRequestHandler<GetProductQuery, IEnumerable<Product>>
    {
        IGroceries _data;

        public GetProductHandler(IGroceries data)
        {
            _data = data;
        }
        public async Task<IEnumerable<Product>> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllProducts());
        }
    }
}
