﻿using MediatR;
using Milestone.Models;
using System.Collections.Generic;

namespace Milestone.Commands
{
    public class AddProductCommand : IRequest<IEnumerable<Product>>
    {
        public Product Product { get; set; }
    }
}
