﻿namespace Milestone.Models
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? OpenInNewWindow { get; set; }
    }
}
