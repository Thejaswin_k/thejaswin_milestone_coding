﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Models;
using Milestone.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class GetOrderInfoHandler : IRequestHandler<GetOrderInfoQuery, ProductOrder>
    {
        IGroceries _data;

        public GetOrderInfoHandler(IGroceries data)
        {
            _data = data;
        }

        public async Task<ProductOrder> Handle(GetOrderInfoQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetOrderInfo(request.OrderId));
        }
    }
}
