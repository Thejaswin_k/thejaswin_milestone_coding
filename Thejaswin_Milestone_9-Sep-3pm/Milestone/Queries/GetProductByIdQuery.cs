﻿using MediatR;
using Milestone.Models;

namespace Milestone.Queries
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int Id { get; set; }
    }
}
