﻿using Milestone.Models;
using System.Collections.Generic;
namespace Milestone.BusinessLayer.Services
{
    public interface IGroceries
    {
        IEnumerable<Product> GetAllProducts();  //Done
        IEnumerable<MenuBar> GetAllCategories();   //Done
        IEnumerable<Product> AddProduct(Product newProduct); //Done
        Product GetProductById(int id); //Done
        ProductOrder GetOrderInfo(int id); //Done
        ProductOrder PlaceOrder(int userId, int productId); //Done
    }
}
